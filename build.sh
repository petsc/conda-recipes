#!/bin/bash
while :; do
    case $1 in
        --mpi|-mpi)
            MPI="$2"
            shift;;
        --mpi=?*)
            MPI="${1#*=}";;
        --python|-python)
            PYARG="--python $2"
            shift;;
        --python=?*)
            PYARG="--python ${1#*=}";;
        *)
            break
    esac
    shift
done
MPI=${MPI-mpich}

set -e -x

for mpi in $(echo $MPI | tr "," "\n"); do
    export MPI=$mpi
    conda build $@ $mpi
    conda build $@ petsc
    conda build $@ slepc
    conda build $@ $PYARG mpi4py
    conda build $@ $PYARG petsc4py
    conda build $@ $PYARG slepc4py
done
