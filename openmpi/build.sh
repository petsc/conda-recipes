#!/bin/bash
export CC=${CC-cc}
export CXX=${CXX-c++}
./configure \
  --disable-mpi-fortran \
  --disable-dependency-tracking \
  --prefix=$PREFIX

sedinplace() { [[ $(uname) == Darwin ]] && sed -i "" $@ || sed -i"" $@; }
sedinplace s%--prefix=$PREFIX%--prefix=\$PREFIX%g opal/include/opal_config.h
sedinplace s%-I$(dirname $SRC_DIR)%-I%g           opal/include/opal_config.h
sedinplace /-DOMPI_MSGQ_DLL=/d                    ompi/debuggers/Makefile

make -j $CPU_COUNT
make install
