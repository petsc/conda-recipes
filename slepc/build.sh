#!/bin/bash

export PETSC_DIR=$PREFIX
export SLEPC_DIR=$SRC_DIR
export SLEPC_ARCH=installed-arch-conda-c-opt

./configure \
  --prefix=$PREFIX

sedinplace() { [[ $(uname) == Darwin ]] && sed -i "" $@ || sed -i"" $@; }
sedinplace s%$SLEPC_DIR%\${SLEPC_DIR}%g $SLEPC_ARCH/include/slepc*.h
sedinplace s%$PETSC_DIR%\${PETSC_DIR}%g $SLEPC_ARCH/include/slepc*.h

make
make install

if [[ $(uname) == Darwin ]];
then
    library=$PREFIX/lib/lib$PKG_NAME.dylib
    pathlist=$(otool -l $library | grep ' path /' | awk '{print $2}')
    for path in $pathlist; do
        install_name_tool -delete_rpath $path $library
    done
else
    library=$PREFIX/lib/lib$PKG_NAME.so
fi
$RECIPE_DIR/replace-binary.py $(dirname $SRC_DIR) "" $library
$RECIPE_DIR/replace-binary.py \
    $PETSC_DIR/include/petscsys.h \
    \${PETSC_DIR}/include/petscsys.h \
    $library

rm -fr $PREFIX/bin
rm -fr $PREFIX/share
rm -fr $PREFIX/lib/lib$PKG_NAME.*.dylib.dSYM
rm -f  $PREFIX/lib/$PKG_NAME/conf/files
rm -f  $PREFIX/lib/$PKG_NAME/conf/*.py
rm -f  $PREFIX/lib/$PKG_NAME/conf/*.log
rm -f  $PREFIX/lib/$PKG_NAME/conf/RDict.db
rm -f  $PREFIX/lib/$PKG_NAME/conf/*BuildInternal.cmake
find   $PREFIX/include -name '*.html' -delete
